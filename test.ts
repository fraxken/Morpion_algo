/// <reference path="typings/index.d.ts" />

import {structure} from './morpion_algo'

const game : structure = new structure(5);

game.on('log',msg => {
    console.log(msg);
});

game.on('reset',_=> {
    console.log('end of the game!');
});

game.on('play',(player,caseID) => {
    console.log(`${player} play on case => ${caseID}`);
});

game.on('victory',player => {
  console.log(`Player ${player} win the game!`);
});

game.play(1,'fraxken');
game.play(2,'fraxken');
game.play(6,'fraxken');
//game.switch(2,3);
console.log(game.print());
